function []=visual_analysis(input, output, class)
    %the results report : 
    
    subplot(2,2,1)
    %visualize the clustering PCA 2D
    [~,score,~,~,~] = pca(input,'NumComponents',2);
    scatter(score(:,1),score(:,2),17,output,'filled')
    hold on
    title('PCA: High-Dimentional Data Visualization (Clusters)')
    xlabel('1st Principal Component')
    ylabel('2nd Principal Component')
    colormap(parula)

    subplot(2,2,2)
    %visualize the clustering PCA 2D
    [~,score,~,~,~] = pca(input,'NumComponents',2);
    scatter(score(:,1),score(:,2),17,class,'filled')
    hold on
    title('PCA: High-Dimentional Data Visualization(Classes)')
    xlabel('1st Principal Component')
    ylabel('2nd Principal Component')
    colormap(parula)
    
    subplot(2,2,3)
    %visualize the clustering t-SNE 2D
    Y = tsne(input);
    gscatter(Y(:,1),Y(:,2),output)
    title('t-SNE: High-Dimentional Data Visualization')
    legend("cluster1", "cluster2","cluster3","cluster3")
    
    subplot(2,2,4)
    %distrinbution of the classes of this dataset
    [h] = histogram(class,'FaceColor','green');
    title('Class Distribution in The Dataset')
    xlabel('Class number')
    ylabel('Frequency')
    
    
    
    %%%%print the evaluation measures
    acc=                Accuracy(   output,  class);
    error_rate=         Error_Rate( output,  class);
    entropy=            Entropy(    output,  class);
    purity=             Purity (    output,  class);
    silhouettee_score=  mean(silhouette(input,output));
    [precision,recall,f_score]=f_measure(output, class);
    fprintf('- The accuracy of the model is %f with error rate  %f\n- The entropy score is %f and purity is %f\n- The f score is %f with precision %f  and recall %f\n- The silhouette score is %f\n',acc,error_rate,entropy,purity,f_score,precision,recall,silhouettee_score );
        
    

    
end
