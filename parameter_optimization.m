
function [solution]=parameter_optimization(tf_idf ,class,k, model_name,dataset_ind)
    datasets=["CSTR","classic4","tr12","tr41","wap"];
    if model_name == "kmean"
    %	using kmean clustering 
        [best_init_cent_loc,best_Max_Iter,best_dist_funct]=kmean_parameter_opt(tf_idf,class,k,model_name,datasets(dataset_ind));
        solution=kmeans(tf_idf,k,'Distance',best_dist_funct, 'MaxIter',best_Max_Iter,'Start',best_init_cent_loc);
        fprintf("the tuned parateter variable for kmeans are : \n%s distance\n%d\nmaximum iterations\n%s to intialize the location of the centroids",best_dist_funct,best_Max_Iter,best_init_cent_loc);



    elseif model_name == "kmedoid"
    %	using kmodiod clustering 
        [best_dist_funct]=kmed_parameter_opt(tf_idf,class,k,model_name,datasets(dataset_ind));
        solution=kmedoids(tf_idf,k,'Distance',best_dist_funct);
        fprintf("the tuned parateter variable for kmedoids is : \n%s distance\n",best_dist_funct);


    elseif model_name == "dbscan"
        [epsilon, minPts, distance_funct]=dbscan_parameter_opt(tf_idf ,class,model_name,datasets(dataset_ind));
        solution=dbscan(tf_idf,epsilon,minPts,'Distance',distance_funct);
        fprintf("the tuned parateter variable for dbscan is : \n%s distance\n",distance_funct);

    end
end