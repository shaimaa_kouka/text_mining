clear;clc;
%   Preprocess (RUN ONLY ONCE)
%preprocess_and_save_alldata()


%   Load the data (each dataset is struct and have 2 fields tf_idf and class)
load corpus
 

%   Perform number of k-cluster analysis
tf_idf_arr={CSTR.tf_idf ,classic4.tf_idf, tr12.tf_idf,tr41.tf_idf,wap.tf_idf};
class_arr={CSTR.class ,classic4.class ,tr12.class,tr41.class,wap.class};
%k_clusters_analysis(tf_idf_arr,class_arr)
 
 
%   select the dataset
[ds]=input('Please select one of the following datasets:(enter the number of the dataset as shown) and the k clusters\n1)- CSTR\n2)- Classic4\n3)- tr12\n4)- tr41\n5)- wap\n');
[k]=input('Please select k clusters\n');
disp("loading the dataset")


%	select clustering model
tf_idf=tf_idf_arr(ds);
tf_idf=tf_idf{1,1};
class=class_arr(ds);
class=class{1,1};
disp("choosing the most suitable clustering technique")
model_name=choose_model(tf_idf ,class,k);
fprintf("%s clustering technique is chosen\n",model_name);
%output: function + k


%  parameter tunning analysis
%perform hyperparamter tuning, each algo has its won
[solution]=parameter_optimization(tf_idf ,class,k, model_name,ds);
disp(mean(silhouette(tf_idf,solution,'cosine')));
%parameter_tuning_function



%   visualize the results
visual_analysis(tf_idf, solution, class);


