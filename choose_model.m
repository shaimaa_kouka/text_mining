function [model_name]=choose_model(tf_idf ,class,k)

    n=30;
 
    for i=1:n
            
        %using kmean clustering 
        kmean_sol=kmeans(tf_idf,k,'Distance','cosine');
        kmean_acc(1,i)      = Accuracy      (kmean_sol,  class);
        kmean_error(1,i)    = Error_Rate    (kmean_sol,  class);
        kmean_entropy(1,i)  = Entropy       (kmean_sol, class);
        kmean_purity(1,i)   = Purity        (kmean_sol,  class);
        kmean_s(1,i)        = mean(silhouette(tf_idf,kmean_sol,'cosine'));
        [kmean_p(1,i),kmean_r(1,i),kmean_f_score(1,i)]=f_measure(kmean_sol, class);
        
              
        
        %using kmodiod clustering 
        kmed_sol=kmedoids(tf_idf,k,'Distance','cosine');
        kmed_acc(1,i)      = Accuracy      (kmed_sol,  class);
        kmed_error(1,i)    = Error_Rate    (kmed_sol,  class);
        kmed_entropy(1,i)  = Entropy       (kmed_sol, class);
        kmed_purity(1,i)   = Purity        (kmed_sol,  class);
        kmed_s(1,i)        = mean(silhouette(tf_idf,kmed_sol,'cosine'));
        [kmed_p(1,i),kmed_r(1,i),kmed_f_score(1,i)]=f_measure(kmed_sol, class);
        



        %using dbscan clustering
        db_sol=dbscan(tf_idf,k,4*k);
        db_acc(1,i)      = Accuracy      (db_sol,  class);
        db_error(1,i)    = Error_Rate    (db_sol,  class);
        db_entropy(1,i)  = Entropy       (db_sol, class);
        db_purity(1,i)   = Purity        (db_sol,  class);
        db_s(1,i)        = mean(silhouette(tf_idf,db_sol,'cosine'));
        [db_p(1,i),db_r(1,i),db_f_score(1,i)]=f_measure(db_sol, class);
            


    end
    
   % calculate the std of kmean performance
   eva_mat(1,1)=mean(kmean_acc);
   eva_mat(1,2)=mean(kmean_error);
   eva_mat(1,3)=mean(kmean_entropy);
   eva_mat(1,4)=mean(kmean_purity);
   eva_mat(1,5)=mean(kmed_p);
   eva_mat(1,6)=mean(kmean_r);
   eva_mat(1,7)=mean(kmean_f_score);
   
   % calculate the std of kmeadoid performance
   eva_mat(2,1)=mean(kmed_acc);
   eva_mat(2,2)=mean(kmed_error);
   eva_mat(2,3)=mean(kmed_entropy);
   eva_mat(2,4)=mean(kmed_purity);
   eva_mat(2,5)=mean(kmed_p);
   eva_mat(2,6)=mean(kmed_r);
   eva_mat(2,7)=mean(kmed_f_score);
   
   % calculate the std of dbscan performance
   eva_mat(3,1)=mean(db_acc);
   eva_mat(3,2)=mean(db_error);
   eva_mat(3,3)=mean(db_entropy);
   eva_mat(3,4)=mean(db_purity);
   eva_mat(3,5)=mean(db_p);
   eva_mat(3,6)=mean(db_r);
   eva_mat(3,7)=mean(db_f_score);
   
    
    t=table();
    t.Clustering=["Kmean"; "K-Medoid"; "DBScan"];
    t.Accuracy=   eva_mat(:,1);
    t_Error_rate= eva_mat(:,2);
    t.Entropy=    eva_mat(:,3);
    t.Purity=     eva_mat(:,4);
    t.Precision=  eva_mat(:,5);
    t.Recall=     eva_mat(:,6);
    t.F1_score=   eva_mat(:,7);
    location= fullfile('gridsearch','_best_clustering_model_eva.xlsx');
    writetable(t,location,'Sheet',1);
    ind= find(eva_mat(:,7)== max(eva_mat(:,7)));
    if(ind== 1)
        model_name= "kmean";
    elseif (ind== 2)
        model_name= "kmedoid";
    elseif (ind== 3)
        model_name= "dbscan";
    end

    
end



% notes :
%tf-idf is d x w
%class is  d x 1
