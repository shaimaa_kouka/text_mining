function [ best_init_cent_loc,best_Max_Iter,best_dist_funct]=kmean_parameter_opt(tf_idf,class,k,model_name,dataset_name)
    %distance_funct_list=["sqeuclidean","cityblock","cosine","correlation"];
    %maxIterations =[100,1000,10000];
    %initial_centroid_position=["cluster","plus","sample","uniform","numeric matrix","numeric array"];
    data = readtable("gridsearch/kmean_best_parameters.xlsx",'ReadVariableNames', true,'VariableNamingRule','preserve');
    
    
    eva_mat=[];
   for row=1:height(data)
        n=30;
        
        %extract parameters
        init_centroid_loc=char(table2cell(data(row,1)));
        max_iter=table2array(data(row,2));
        distance_funct= char(table2cell(data(row,3)));
        
       
        
        %run 30 times (for exploration purposes)
        for i=1:n

            %using dbscan clustering
            kmean_sol=kmeans(tf_idf,k,'Distance',distance_funct, 'MaxIter',max_iter,'Start',init_centroid_loc);
            kmean_acc(1,i)      = Accuracy      (kmean_sol, class);
            kmean_error(1,i)    = Error_Rate    (kmean_sol, class);
            kmean_entropy(1,i)  = Entropy       (kmean_sol, class);
            kmean_purity(1,i)   = Purity        (kmean_sol, class);
            kmean_s(1,i)        = mean(silhouette(tf_idf,kmean_sol));
            [kmean_p(1,i),kmean_r(1,i),kmean_f_score(1,i)]=f_measure(kmean_sol, class);

        end
    
       % calculate the std of dbscan performance
       eva_mat(row,1)=mean(kmean_acc);
       eva_mat(row,2)=mean(kmean_error);
       eva_mat(row,3)=mean(kmean_entropy);
       eva_mat(row,4)=mean(kmean_purity);
       eva_mat(row,5)=mean(kmean_p);
       eva_mat(row,6)=mean(kmean_r);
       eva_mat(row,7)=mean(kmean_f_score);
    
   end
   t=table();
    %add the parameters values
    t.init_cent_loc = string(table2cell(data(:, 1)));
    t.Max_Iter=table2array(data(:, 2));
    t.dist_funct=string(table2cell(data(:, 3)));
    %add the performance of each set of paramaters
    t.Accuracy=   eva_mat(:,1);
    t_Error_rate= eva_mat(:,2);
    t.Entropy=    eva_mat(:,3);
    t.Purity=     eva_mat(:,4);
    t.Precision=  eva_mat(:,5);
    t.Recall=     eva_mat(:,6);
    t.F1_score=   eva_mat(:,7);
    location= fullfile('gridsearch',dataset_name+"_"+model_name+"_best_distance_function_eva.xlsx");
    writetable(t,location,'Sheet',1);
    ind= find(eva_mat(:,7)== max(eva_mat(:,7)));
    best_init_cent_loc= t{ind,1};
    best_Max_Iter= t{ind,2};
    best_dist_funct= t{ind,3};
 
    
end