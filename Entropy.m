 function [entropy]=Entropy(cluster,class)
    % create structure of clusters
    cell_cluster= cell(length(unique(cluster)));
    for i=1: length(cluster)
        if cluster(i) > 0 %avoid - val in the dbscan output
            index = length([cell_cluster{cluster(i),:}]);
            cell_cluster{cluster(i),index+1} = class(i);
        end
    end
    entropy4cluster=zeros(height(cell_cluster));
    for j=1:height(cell_cluster)
        %run for each cluster to compute entropy of a cluster
        [group_counts, group_name]=groupcounts([cell_cluster{j,:}]');
        p=group_counts/length([cell_cluster{j,:}]);
        entropy = p.*log2(p); 
        entropy4cluster(j)=sum(entropy);
        length_of_cluster(j,1)=length([cell_cluster{j,:}])/height(cluster); 

    end
    entropy=sum(entropy4cluster .* length_of_cluster);
    entropy=entropy(1);
 end
 
 % test case
 % class=[1;1;1;1;1;1;1;1;2;2;2;2;2;3;3;3;3];
 % clust=[1;1;1;1;1;2;3;3;1;2;2;2;2;2;3;3;3];
 % total entropy is -.956
 % source: https://stats.stackexchange.com/questions/338719/calculating-clusters-entropy-python
 % source : https://stackoverflow.com/questions/35709562/how-to-calculate-clustering-entropy-a-working-example-or-software-code