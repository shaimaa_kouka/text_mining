function[acc]=Accuracy(output,target)
    acc_sum=0;
    for i=1: length(output)
        if output(i) == target(i)
            acc_sum=acc_sum+1;
        end
    end
    acc = acc_sum/length(output);
    acc = round(acc,5);
end
%input class=[1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3];
%      cluster=[1,1,1,1,1,2,3,3,1,2,2,2,2,2,3,3,3];
%output 0.705