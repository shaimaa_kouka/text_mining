% to do next

% [done] try on the rest of the testbenches
% [done] calculate all extrinsic measures 
% [DONE] build table of parameter tunning of each model to find the best performance
%       [done] run it 10 times and take min, max, average performance,(cuz 30 is too much time)    
% [Done] use sihoutte algo to determine k cluster number
% [Done] visualize more 
%  
%
%     
%     
% sofisticated:
% [not applicable] use cross validation
% [not applicable] freeze the model's clusters
% [] implement knee detection https://github.com/arvkevi/kneed
% [] try matlab markup for better documentation 

%dont forget to do the following :
%1. save all figures 
%2. print log msgs so i know where are we now!

%Note:you can save the results and distance for each RUN in order to
%compute: min,max, avearge distance

%%%%%%%%%%
%k-mean parameters each with its variants
% 1. distance : cityblock , cosine , correlation, hamming
% 2. EmptyAction — Action to take if cluster loses all member observations:
% error , drop , singelton
% 3. MaxIter : (default)100
% 4. Replicates : number of times to repeat clustering using new initial cluster
% centroid position , (default) 1
% 5. Start (choosing initial cluster centroid positions), cluster, plus, sample uniform
% , numeric matrix, numeric array


%%%%%%%%%%%
%k-medoid parameters each with its variants
% 1. algorithm to find medoids: pam, small, clara, large
% 2. distance : sqEuclidean,euclidean,seuclidean, cityblock,minkowski,chebychev
% mahalanobis, cosine,correlation,spearman,hamming,jaccard
% 3. Options — Options to control iterative algorithm to minimize fitting criteria :
% display, maxIter, useParallel, UseSubstreams, Streams
% 4. Replicates : number of times to repeat clustering using new initial cluster
% centroid position , (default) 1
% 5. NumSamples — Number of samples to take from data when executing clara algorithm
% 6. PercentNeighbors — Percent of data set to examine using large algorithm
% 5. Start (choosing initial cluster centroid positions), cluster, plus, sample , matrix






