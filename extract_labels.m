function [label_n] = extract_labels(labels)
label=[labels(1)];
for i=2:length(labels)
    if find( label == labels(i))
    else
        label(end+1)=labels(i);   
    end
end
for x=1:length(labels)
    found = find(label ==labels(x));
    label_n(x) = found(1);
end
label_n=label_n';

end