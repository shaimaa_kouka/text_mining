function []=preprocess_and_save_alldata()

    corpus=struct('CSTR',[],'classic4',[],'tr12',[],'tr41',[],'wap',[]);
    files = fieldnames(corpus);%output is cell type
    for file_ind=1:length(files)
        
        filename = fullfile('corpus',string(files(file_ind))+'.csv');
  
        %log mesg that the database is currently under processes
        fprintf('the dataset %s is currently under processes......\n',string(files(file_ind)));
        
        % calculate tf-idf
        [tf_idf,labels,~] =generate_tf_idf(filename);
        
        %prepare the target vector
        target= extract_labels(labels);
        
        
        %save it to the corpus
        corpus.(string(files(file_ind))).tf_idf=tf_idf;
        corpus.(string(files(file_ind))).class =target;
        
        %log mesg that the database is processes
        fprintf('the dataset %s is successfull processed and stored\n\n',string(files(file_ind)));
     
    end
    save('corpus.mat','-struct','corpus');

end