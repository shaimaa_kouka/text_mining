function []=k_clusters_analysis(tf_idf_array, class_array)
    
    ind=0;f=figure('visible','on');%for the subplot
    files=["CSTR","classic4","tr12","tr41","wap"];
    for i=1:length(class_array)
        
        %load current data
        tf_idf=cell2mat(tf_idf_array(i));
        class=cell2mat(class_array(i));
        
        %k-clusters analysis
        n=round(sqrt(length(class)/2));% imperical method to find k
        fprintf('the %s is beging analyzed\n',files(i));
        inertia=[];sil=[];
        for k=1:n
            %kmean - elbow
            [kmean_sol, ~, kmean_sumdist]=kmeans(tf_idf, k);
            inertia(k,1)=sum(kmean_sumdist);

            %kmean - silhoutte
            [kmean_s] = silhouette(tf_idf,kmean_sol);
            sil(k,1)= mean(kmean_s);

            %k-medoid - elbow
            [kmed_sol, ~, kmed_sumdist]=kmedoids(tf_idf, k);
            inertia(k,2)=sum(kmed_sumdist);

            %kmedoid - silhoutte
            [kmed_s] = silhouette(tf_idf,kmed_sol);
            sil(k,2)= mean(kmed_s);
        end
        
        
        % plot the data
        k=[1:n]';
        ind=ind+1;
        subplot(length(files),2,ind)
        plot(k, inertia(:,1),k, inertia(:,2))
        legend("K-Mean", "K-Medoid")
        title(files(i)+': Number of Clusters VS Distance')
        xlabel("Number of Clusters (K)")
        ylabel("Distance")
        grid on
        grid minor
        
        
        ind=ind+1;
        subplot(length(files),2,ind)
        plot(k, sil(:,1),k, sil(:,2))
        legend("K-Mean", "K-Medoid")
        title(files(i)+': Number of Clusters VS Silhouette Coefficients ')
        xlabel("Number of Clusters (K)")
        ylabel("Silhouette Score")
        grid on
        grid minor
    end
    saveas(f,'k clusters analysis.fig')
    
    
end
% k cluster is only for dbscan cuz they are herarical while dbscan is
% density based
% note that silhouette always starts with 2 not 1 
% source : https://towardsdatascience.com/silhouette-method-better-than-elbow-method-to-find-optimal-clusters-378d62ff6891
