function [ER]=Error_Rate(output, target)
    t_misclassified=0;
    for i=1:length(output)
        %equation (1/n)(num of wrongly classified points)
        if output(i) ~= target(i)
            
            t_misclassified = t_misclassified+1;
        end
    end
    ER = (t_misclassified/length(output));
    ER= round(ER,5);
end

%input class=[1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3];
%      cluster=[1,1,1,1,1,2,3,3,1,2,2,2,2,2,3,3,3];
%output 0.29