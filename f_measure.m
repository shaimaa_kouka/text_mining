function [total_precision, total_recall, fmeasure]=f_measure(cluster, class)
    % create structure of clusters
    cluster_num = length(unique(cluster));class_num=length(unique(class));
    size=max(cluster_num,class_num);
    mat_cluster=zeros(size,size);
    %             mat2cell(A,rows,columns)
    cell_cluster= mat2cell(mat_cluster, ones(size,1), ones(size,1));
    outlier=zeros(length(unique(class)),1);
    for i=1:length(cluster)
        if cluster(i) > 0
            cell_cluster{cluster(i),class(i)}=1+cell_cluster{cluster(i),class(i)};
        else
            outlier(class(i),1)=outlier(class(i),1)+1;
        end
    end
    
    fmeasure=0;total_precision=0;total_recall=0;
    for i=1:length(unique(cluster))
        
        if cell_cluster{i,i} == 0
            f_cluster=0;
            precision =0;
            recall = 0;
        else
            precision = cell_cluster{i, i}/sum([cell_cluster{i,:}]);
            recall = cell_cluster{i, i}/(sum([cell_cluster{:,i}]) + outlier(i,1));
            f_cluster = (2*precision*recall)/(precision+recall);
        end
        %the full size of cluster is including all points regardless if it
        %is outliers or not
        weight_of_cluster=sum([cell_cluster{i,:}])/length(cluster);
     
        fmeasure = fmeasure+(f_cluster*weight_of_cluster);
        total_precision = total_precision +(weight_of_cluster*precision);
        total_recall = total_recall +(weight_of_cluster*recall);
        
    end
    
end
%test case 
%input class=[1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3];
%      cluster=[1,1,1,1,1,2,3,3,1,2,2,2,2,2,3,3,3];
%output = 0.7146
%https://towardsdatascience.com/evaluating-clustering-results-f13552ee7603
%https://sci-hub.yncjkj.com/10.1016/j.asoc.2019.106002
%https://stats.stackexchange.com/questions/204626/2-different-f1-measure-to-calculate-clustering-performance-which-one-is-correc