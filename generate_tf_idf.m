function [tf_idf,labels,words] =generate_tf_idf(filename)
    
    %read data and seperate array of labels , words, tf
    data = readtable(filename,'ReadVariableNames', true,'VariableNamingRule','preserve');

    if ismember({'class_atr'},data.Properties.VariableNames)
        labels= convertCharsToStrings(data.class_atr);% take the label col
        data.class_atr = []; % remove label col
    else
        labels= convertCharsToStrings(data.CLASS_LABEL);% take the label col
        data.CLASS_LABEL = []; % remove label col
    end
    
    words=convertCharsToStrings(data.Properties.VariableNames); %take all words (headers)
    data = table2array(data);
    
    %calculate TF
    for t=1:width(data)
        t_occur=0;
        for d=1:height(data)
           tf(d,t)=data(d,t)/sum(data(d,:));
            if data(d,t) > 0
                t_occur = t_occur + 1;
            end
        end
    %calculate idf
        if t_occur ==0 
            idf(1,t)=0;
        else
            idf(1,t) = log(height(data)/t_occur);
        end
        
    end
    %calculate tf-idf
    for d=1:height(data)
        tf_idf(d,:) =tf(d,:) .* idf;
    end
end


%info
% tf is (docs x terms)
% idf is (1 x terms)
% tf * idf = <n from 1-last doc><.* is multiplication element by element> tf(n,:) .* idf 
% test case : https://www.analyticsvidhya.com/blog/2020/02/quick-introduction-bag-of-words-bow-tf-idf/