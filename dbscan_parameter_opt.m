function [epsilon, minPts, distance_funct]=dbscan_parameter_opt(tf_idf ,class, model_name, dataset_name)
    distance_funct_list=["euclidean", "squaredeuclidean","seuclidean","cityblock","minkowski","chebychev","cosine","correlation","jaccard","spearman"];
    %minPts = width(tf_idf);
    minPts =10;
    [~, distance]=knnsearch(tf_idf,tf_idf, 'K', minPts);
    distance=sort(distance);
    k=[1:length(distance)]';
    
    %draw the Knee plot
    plot(k,distance(:,2),'r.')
    title('Knee Graph Analysis For Tuning Epsilon Value')
    xlabel(minPts+" k-nearest neighbor Distance" )
    ylabel('Number of Points')
    grid on
    grid minor
    epsilon=input('Please choose the epsilon value based on the Knee analysis !\n');
    eva_mat=[];
   for func_ind=1:length(distance_funct_list)
        n=30;
 
        for i=1:n

            %using dbscan clustering
            db_sol=dbscan(tf_idf,epsilon,minPts,'Distance',distance_funct_list(func_ind));
            db_acc(1,i)      = Accuracy      (db_sol,  class);
            db_error(1,i)    = Error_Rate    (db_sol,  class);
            db_entropy(1,i)  = Entropy       (db_sol, class);
            db_purity(1,i)   = Purity        (db_sol,  class);
            db_s(1,i)        = mean(silhouette(tf_idf,db_sol));
            [db_p(1,i),db_r(1,i),db_f_score(1,i)]=f_measure(db_sol, class);

        end
    
       % calculate the std of dbscan performance
       eva_mat(func_ind,1)=mean(db_acc);
       eva_mat(func_ind,2)=mean(db_error);
       eva_mat(func_ind,3)=mean(db_entropy);
       eva_mat(func_ind,4)=mean(db_purity);
       eva_mat(func_ind,5)=mean(db_p);
       eva_mat(func_ind,6)=mean(db_r);
       eva_mat(func_ind,7)=mean(db_f_score);
    
   end
   t=table();
    t.Distance_Function=["euclidean"; "squaredeuclidean";"seuclidean";"cityblock";"minkowski";"chebychev";"cosine";"correlation";"jaccard";"spearman"];
    t.Accuracy=   eva_mat(:,1);
    t_Error_rate= eva_mat(:,2);
    t.Entropy=    eva_mat(:,3);
    t.Purity=     eva_mat(:,4);
    t.Precision=  eva_mat(:,5);
    t.Recall=     eva_mat(:,6);
    t.F1_score=   eva_mat(:,7);
    location= fullfile('gridsearch',dataset_name+"_"+model_name+"_best_distance_function_eva.xlsx");
    writetable(t,location,'Sheet',1);
    ind= find(eva_mat(:,7)== max(eva_mat(:,7)));
    distance_funct= distance_funct_list(ind(1));
    
end