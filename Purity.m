 function [purity]=Purity(output,target)
    %count the maximum category in each cluster and add it over the number
    %divide the array to k clusters and keep its actual label
     cluster_num = unique(output);
        sum_of_max=0;
        for label=1:length(cluster_num)
            current_matrix=[0];
            for i=1:height(target)
                if output(i,1) == cluster_num(label) && output(i,1) > 0
                    current_matrix(end+1,1)=target(i,1);
                end
            end
            [group_count,~]=groupcounts(current_matrix);
            sum_of_max=max(group_count)+sum_of_max;
        end
        purity=sum_of_max/length(target);
 end
%test case : 
%input: target=[1;1;1;1;1;1;2;2;2;2;2;2;2;3;3;3;3;3];  output=[1;1;1;1;2;3;1;2;2;,2;2;2;3;1;3,;3;3;3];
%output: purity = 0.7222 

%input class=[1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3];
%      cluster=[1,1,1,1,1,2,3,3,1,2,2,2,2,2,3,3,3];
%output 0.70588
%source :https://towardsdatascience.com/evaluation-metrics-for-clustering-models-5dde821dd6cd