function [best_dist_funct]=kmed_parameter_opt(tf_idf,class,k,model_name,dataset_name)
    %distance=["sqEuclidean","euclidean","seuclidean","cityblock","minkowski","chebychev","mahalanobis","cosine","correlation","spearman","jaccard"]
    %MaxIter=[100, 1000, 10000]
    %Start =["plus","sample","cluster"]
    data = readtable("gridsearch/kmedoid_best_parameters.xlsx",'ReadVariableNames', true,'VariableNamingRule','preserve');
    
    
    eva_mat=[];
   for row=1:height(data)
        n=30;
        disp(row);
        %extract parameters
        %init_centroid_loc=char(table2cell(data(row,3)));
        %max_iter=table2array(data(row,2));
        distance_funct= char(table2cell(data(row,1)));
        
       
        
        %run 30 times (for exploration purposes)
        for i=1:n

            %using dbscan clustering
            kmed_sol=kmedoids(tf_idf,k,'Distance',distance_funct);
            kmed_acc(1,i)      = Accuracy      (kmed_sol, class);
            kmed_error(1,i)    = Error_Rate    (kmed_sol, class);
            kmed_entropy(1,i)  = Entropy       (kmed_sol, class);
            kmed_purity(1,i)   = Purity        (kmed_sol, class);
            kmed_s(1,i)        = mean(silhouette(tf_idf,kmed_sol));
            [kmed_p(1,i),kmed_r(1,i),kmed_f_score(1,i)]=f_measure(kmed_sol, class);

        end
    
       % calculate the std of dbscan performance
       eva_mat(row,1)=mean(kmed_acc);
       eva_mat(row,2)=mean(kmed_error);
       eva_mat(row,3)=mean(kmed_entropy);
       eva_mat(row,4)=mean(kmed_purity);
       eva_mat(row,5)=mean(kmed_p);
       eva_mat(row,6)=mean(kmed_r);
       eva_mat(row,7)=mean(kmed_f_score);
    
   end
   t=table();
    %add the parameters values
    %t.init_cent_loc = string(table2cell(data(:, 1)));
    %t.Max_Iter=table2array(data(:, 2));
    t.dist_funct=string(table2cell(data(:, 1)));
    %add the performance of each set of paramaters
    t.Accuracy=   eva_mat(:,1);
    t_Error_rate= eva_mat(:,2);
    t.Entropy=    eva_mat(:,3);
    t.Purity=     eva_mat(:,4);
    t.Precision=  eva_mat(:,5);
    t.Recall=     eva_mat(:,6);
    t.F1_score=   eva_mat(:,7);
    location= fullfile('gridsearch',dataset_name+"_"+model_name+"_best_distance_function_eva.xlsx");
    writetable(t,location,'Sheet',1);
    ind= find(eva_mat(:,7)== max(eva_mat(:,7)));
    %best_init_cent_loc= t{ind,1};
    %best_Max_Iter= t{ind,2};
    best_dist_funct= t{ind,1};
end